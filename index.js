const app = require('./app');

require('dotenv').config();

const port = process.env.PORT || 5000;

app.listen(port, () => {
    console.log(`REG.CRM Server started on localhost:${port} .... ` )
});