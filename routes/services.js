const express = require('express');
const passport = require('passport');
const router = express.Router();
const servicesController = require('../controller/services');

router.get('/:categoryId', passport.authenticate('jwt', {session: false}), servicesController.getService);
router.post('/', passport.authenticate('jwt', {session: false}), servicesController.createService);
router.patch('/:id', passport.authenticate('jwt', {session: false}), servicesController.updateService);
router.delete('/:id', passport.authenticate('jwt', {session: false}), servicesController.removeService);

module.exports = router;