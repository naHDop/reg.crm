const express = require('express');
const passport = require('passport');
const router = express.Router();
const statisticsController = require('../controller/statistics');


router.get('/overview', passport.authenticate('jwt', {session: false}), statisticsController.overview);
router.get('/analitic', passport.authenticate('jwt', {session: false}), statisticsController.analitic);

module.exports = router;