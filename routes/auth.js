const express = require('express');
const router = express.Router();
const authController = require('../controller/auth');

// localhost:<port>/api/auth/login
router.post('/login', authController.login);
// localhost:<port>/api/auth/register
router.post('/register', authController.register);

module.exports = router;