const express = require('express');
const passport = require('passport');
const upload = require('../middleware/upload');
const router = express.Router();
const categoryController = require('../controller/category');


router.get('/', passport.authenticate('jwt', {session: false}), categoryController.getAllCategorys);
router.get('/:id', passport.authenticate('jwt', {session: false}), categoryController.getCategory);
router.delete('/:id', passport.authenticate('jwt', {session: false}), categoryController.removeCategory);
router.post('/', passport.authenticate('jwt', {session: false}), upload.single('image'), categoryController.createCategory);
router.patch('/:id', passport.authenticate('jwt', {session: false}), upload.single('image'), categoryController.updateCategory);

module.exports = router;