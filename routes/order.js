const express = require('express');
const passport = require('passport');
const router = express.Router();
const orderController = require('../controller/order');

router.get('/', passport.authenticate('jwt', {session: false}), orderController.getAllOrders);
router.post('/', passport.authenticate('jwt', {session: false}), orderController.createOrder);
router.patch('/feedback/:orderId', passport.authenticate('jwt', {session: false}), orderController.updateFeedback);

module.exports = router;