const mongoose = require('mongoose');

const Scheema = mongoose.Schema;

const userScheema = new Scheema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        require: false
    },
    lastName: {
        type: String,
        require: false
    },
    phone: {
        type: Number,
        require: false
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    role: {
        type: String,
        enum: ['client', 'admin', 'support', 'stat'],
        default: 'client'
    }
});

module.exports = mongoose.model('users', userScheema);