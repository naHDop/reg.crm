const mongoose = require('mongoose');

const Scheema = mongoose.Schema;

const servicesScheema = new Scheema({
    name: {
        type: String,
        required: true
    },
    cost: {
        type: Number,
        required: true
    },
    category: {
        ref: 'categories',
        type: Scheema.Types.ObjectId
    },
    user: {
        ref: 'users',
        type: Scheema.Types.ObjectId
    }
});

module.exports = mongoose.model('services', servicesScheema);