const mongoose = require('mongoose');

const Scheema = mongoose.Schema;

const orderScheema = new Scheema({
    date: {
        type: Date,
        default: Date.now
    },
    order: {
        type: Number,
        required: true
    },
    list: [
        {
            name: {
                type: String
            },
            quantity: {
                type: Number
            },
            cost: {
                type: Number
            }
        }
    ],
    user: {
        ref: 'users',
        type: Scheema.Types.ObjectId
    },
    feedback: {
        type: Number,
        required: false,
        enum: [-2, 0, 2]
    }
});

module.exports = mongoose.model('orders', orderScheema);