const mongoose = require('mongoose');

const Scheema = mongoose.Schema;

const categoryScheema = new Scheema({
    name: {
        type: String,
        required: true
    },
    imageSrc: {
        type: String,
        default: ''
    },
    user: {
        ref: 'users',
        type: Scheema.Types.ObjectId
    }
});

module.exports = mongoose.model('categories', categoryScheema);