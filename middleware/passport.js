const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const mongoose = require('mongoose');
const User = mongoose.model('users');

const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET
}

module.exports = passport => {
    passport.use(
        new JwtStrategy(options, async (payload, done) => {
            // payload.userId - из вебтокена, который мы сгенерили
            try {
                const user = await User.findById(payload.userId).select('email id isAdmin role')
                if (user) {
                    done(null, user);
                } else {
                    done(null, false);
                }
            } catch (err) {
                console.log(err);
            }
        })
    );
}