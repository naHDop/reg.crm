const Feedback = require('../models/Feedback');
const errorHendler = require('../utils/errorHendler');
const { Validator } = require('node-input-validator');

module.exports.getFeedbacks = async function (req, res) {
    const categoryId = req.params.categoryId
    const validator = new Validator({categoryId: categoryId}, {
        categoryId: 'required',
    });
    try {
        const matched = await validator.check();
        if (!matched) {
            res.status(422).send(validator.errors);
        } else {
            const feedback = await Feedback.find({
                category: categoryId,
                // user: req.user.id
            });
            res.status(200).json(feedback);
        }
    } catch (err) {
        errorHendler(res, err);
    }
};

module.exports.createFeedback = async function (req, res) {
    const mapParams = {};

    const fieldsMap = {
        title: 'required|string',
        order: 'required|string',
        message: 'require|array',
        user: 'require|string',
    };

    try {
        Object.keys(req.body).map(keys => {
            mapParams[keys] = fieldsMap[keys]
        });

        const title = req.body.title;
        const order = req.body.order;
        const message = req.body.message;
        // const category = req.body.category;
        const user = req.body.user;
        // const userEmail = req.body.userEmail;



        const validator = new Validator(req.body, mapParams);
        const matched = await validator.check();

        if (!matched) {
            
            const service = await new Services({
                name: req.body.name,
                cost: req.body.cost,
                category: req.body.category,
                user: req.user.id
            }).save();
            res.status(201).json(service)
        } else {

        }


    } catch (err) {
        errorHendler(res, err);
    }
};

module.exports.createMessage = async function (req, res) {
    try {
        await Services.remove({_id: req.params.id});
        res.status(200).json({
            message: 'Service is deleting'
        })
    } catch (err) {
        errorHendler(res, err);
    }
};

module.exports.removeFeedback = async function (req, res) {
    try {
        const cervice = await Services.findOneAndUpdate(
            // Ищем
            {_id: req.params.id},
            //  Изменяем
            {$set: req.body},
            // Обновит и вернет
            {new: true}
        );
        res.status(200).json(cervice);
    } catch (err) {
        errorHendler(res, err);
    }
};