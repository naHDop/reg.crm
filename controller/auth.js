const crypting = require('../utils/crypting');
const jwt = require('jsonwebtoken');
const { Validator } = require('node-input-validator')
const errorHendler = require('../utils/errorHendler');

const User = require('../models/User');

module.exports.login = async function(req, res) {
    const v = new Validator(req.body, {
        email: 'required|email',
        password: 'required|minLength:5'
    });

    const email = req.body.email;
    const password = req.body.password;
    const condidate = await User.findOne({email: email});
    const matched = await v.check();

    if (!matched) {
        res.status(422).send(v.errors);
    } else if (condidate) {
        const passResult = crypting.compareSync(password, condidate.password);
        if (passResult) {
            const token = jwt.sign(
                {
                email: condidate.email,
                userId: condidate._id,
                isAdmin: condidate.isAdmin,
                role: condidate.role
                }, 
                process.env.JWT_SECRET, 
                { expiresIn: 60 * 60 }
            );
            res.status(200).json({
                token: `Bearer ${token}`
            })
        } else {
            res.status(401).json({
                message: 'Invalid Password'
            })
        }
    } else {
        res.status(404).json({
            message: 'User not found'
        })
    }
}

module.exports.register = async function(req, res) {
    const mapParams = {};

    const fieldsMap = {
        email: 'required|email',
        password: 'required|minLength:5',
        phone: 'integer|minLength:11|maxLength:15',
        firstName: 'minLength:3|maxLength:25',
        lastName: 'minLength:5|maxLength:30',
        role: ['client', 'admin', 'support', 'stat'],
        isAdmin: 'boolean'
    };

    Object.keys(req.body).map(keys => {
        mapParams[keys] = fieldsMap[keys]
    });

    const email = req.body.email;
    const password = req.body.password;
    const isAdmin = req.body.isAdmin;
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const phone = req.body.phone;
    const role = req.body.role;
    const validator = new Validator(req.body, mapParams);
    const matched = await validator.check();
    const condidate = await User.findOne({email: email});

    if (!matched) {
        res.status(422).send(validator.errors);
    } else if (condidate) {
        res.status(409).json({
            message: 'User already exist'
        });
    } else {
        const user = new User({
            email: email,
            password: crypting.HashedSync(password),
            isAdmin: isAdmin,
            firstName: firstName,
            lastName: lastName,
            phone: phone,
            role: role
        });
        try {
            await user.save();
            res.status(201).json(user);
        } catch (err) {
            errorHendler(res, err);
        }
    }
};