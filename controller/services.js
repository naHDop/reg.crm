const Services = require('../models/Services');
const errorHendler = require('../utils/errorHendler');

module.exports.getService = async function (req, res) {
    try {
        const service = await Services.find({
            category: req.params.categoryId,
            // user: req.user.id
        })
    res.status(200).json(service);
    } catch (err) {
        errorHendler(res, err);
    }
};

module.exports.createService = async function (req, res) {
    try {
        const service = await new Services({
            name: req.body.name,
            cost: req.body.cost,
            category: req.body.category,
            user: req.user.id
        }).save();
        res.status(201).json(service)

    } catch (err) {
        errorHendler(res, err);
    }
};

module.exports.removeService = async function (req, res) {
    try {
        await Services.remove({_id: req.params.id});
        res.status(200).json({
            message: 'Service is deleting'
        })
    } catch (err) {
        errorHendler(res, err);
    }
};

module.exports.updateService = async function (req, res) {
    try {
        const cervice = await Services.findOneAndUpdate(
            // Ищем
            {_id: req.params.id},
            //  Изменяем
            {$set: req.body},
            // Обновит и вернет
            {new: true}
        );
        res.status(200).json(cervice);
    } catch (err) {
        errorHendler(res, err);
    }
};