const moment = require('moment');
const Order = require('../models/Order');
const errorHendler = require('../utils/errorHendler')

module.exports.overview = async function(req, res) {
    try {
        const allOrders = await Order.find(
            // {user: req.user.id}
            ).sort({date: 1});
        const ordersMap = getOrdersMap(allOrders);
        const yesterdayOrders = ordersMap[moment().add(-1, 'd').format('DD.MM.YYYY')] || []

        // Кол-во заказов вчера
        const yesterdayOrdersCount = yesterdayOrders.length;
        // Кол-во заказов
        const ordersCount = allOrders.length;
        // Кол-во двней
        const daysCount = Object.keys(ordersMap).length;
        // ЗАказов в день (округляем)
        const ordersCountPerDay = (ordersCount / daysCount).toFixed(0);
        // Процент для кол-ва заказов
        // ((заказов_вчера / заказов_в_день) - 1) * 100
        const percentCountOrders = (
            (( yesterdayOrdersCount / ordersCountPerDay ) - 1) * 100
            ).toFixed(2);
        // Общая выручка
        const totalGain = calculatePrice(allOrders);
        // Выручка в день 
        const totalGainPerDay = totalGain / daysCount;
        // Выручка за вчера
        const yesterdayGain = calculatePrice(yesterdayOrders);
        // Процент выручки
        const gainPercent = (
            (( yesterdayGain / totalGainPerDay ) - 1) * 100
            ).toFixed(2);
        // Сравнение выручи
        const compareGain = (yesterdayGain - totalGainPerDay).toFixed(2);
        // Сравнение кол-ва заказов
        const compareOrders = ( yesterdayOrdersCount - ordersCountPerDay ).toFixed(2);

        res.status(200).json({
            gain: {
                percent: Math.abs(+gainPercent),
                compare: Math.abs(+compareGain),
                yesterday: +yesterdayGain,
                isHigher: +gainPercent > 0
            },
            orders: {
                percent: Math.abs(+percentCountOrders),
                compare: Math.abs(+compareOrders),
                yesterday: +yesterdayOrdersCount,
                isHigher: +percentCountOrders > 0
            }
        })
    } catch (err) {
        errorHendler(res, err);
    }
}

module.exports.analitic = async function(req, res) {
    try {
        const allOrders = await Order.find(
            // {user: req.user.id}
            ).sort({date: 1});
        const ordersMap = getOrdersMap(allOrders);

        const avarage = +(calculatePrice(allOrders) / Object.keys(ordersMap).length).toFixed(2);

        const chart = Object.keys(ordersMap).map(label => {
            // label == 05.07.2019
            const gain = calculatePrice(ordersMap[label]);

            const order = ordersMap[label].length;

            return {label, gain, order}
            // return {
            //     label: label,
            //     order: order,
            //     gain: gain
            // }
        });

        res.status(200).json({avarage, chart})

        // res.status(200).json({
        //     avarage: +avarage,
        //     chart: chart
        // })
    } catch (err) {
        errorHendler(res, err)
    }
}

function getOrdersMap(orders = []) {
    const daysOrder = {}

    orders.forEach(order => {
        const date = moment(order.date).format('DD.MM.YYYY');

        if (date === moment().format('DD.MM.YYYY')) {
            return
        }

        if (!daysOrder[date]) {
            daysOrder[date] = []
        }

        daysOrder[date].push(order)
    });
    return daysOrder
}

function calculatePrice(orders = []) {
    return orders.reduce((total, order) => {
        const orderCost = order.list.reduce((orderTotal, item) => {
            return orderTotal += item.cost * item.quantity
        }, 0)
    return total += orderCost
    }, 0)
}