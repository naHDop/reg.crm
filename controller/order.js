const Order = require('../models/Order');
const errorHendler = require('../utils/errorHendler');

// GET localhost:8080/api/order?offset=2&limit=5
module.exports.getAllOrders = async function(req, res) {
    const query = {};

    if (
        req.user.role !== 'admin' && 
        req.user.role !== 'stat' && 
        req.user.role !== 'support'
        ) {
        query.user = req.user.id
    }

    if (req.query.start) {
        query.date = {
            // больше или равно
            $gte: req.query.start
        }
    }
    if (req.query.end) {
        if (!query.date) {
            query.date = {}
        }
        query.date['$lte'] = req.query.end;
    }
    if (req.query.order) {
        query.order = +req.query.order
    }

    try {
        const oreders = await Order
            .find(query)
            .sort({date: -1})
            .skip(+req.query.offset)
            .limit(+req.query.limit);

        res.status(200).json(oreders)
    } catch (err) {
        errorHendler(res, err);
    }
}

module.exports.createOrder = async function(req, res) {
    try {
        const lastOrder = await Order
            .findOne(
                // for uniq id for all users
                // {user: req.user.id}
            )
            .sort({date: -1});

        let maxOrder = lastOrder ? lastOrder.order : 0;

        const order = await new Order({
            list: req.body.list,
            user: req.user.id,
            order: maxOrder + 1
        }).save();

        res.status(201).json(order);
    } catch (err) {
        errorHendler(res, err);
    }
}

module.exports.updateFeedback = async function(req, res) {
    let updated = {
        feedback: req.body.feedback
    };
    try {
        const lastOrder = await Order.findOneAndUpdate(
            {_id: req.params.orderId},
            {$set: updated},
            {new: true}
            )
        res.status(201).json(lastOrder);
    } catch (err) {
        errorHendler(res, err);
    }
}