const Category = require('../models/Category');
const Services = require('../models/Services');
const errorHendler = require('../utils/errorHendler');
const { Validator } = require('node-input-validator');

module.exports.getAllCategorys = async function(req, res) {
    const user = req.user.id
    try {
        const categorys = await Category.find(
            // {user}
        );
        res.status(200).json(categorys)
    } catch (err) {
        errorHendler(res, err);
    }
}

module.exports.getCategory = async function(req, res) {
    const id = req.params.id;
    console.log(id);
    const v = new Validator({id: id}, {
        id: 'required',
    });
    try {
        const matched = await v.check();
        if (!matched) {
            res.status(422).send(v.errors);
        } else {
            const category = await Category.findById(req.params.id);
            res.status(200).json(category)
        }
    } catch (err) {
        errorHendler(res, err);
    }
}

module.exports.removeCategory = async function(req, res) {
    const id = req.params.id;
    const v = new Validator({id: id}, {
        id: 'required',
    });
    try {
        const matched = await v.check();
        if (!matched) {
            res.status(422).send(v.errors);
        } else {
            const categoryId = id;
            await Category.remove({_id: categoryId});
            await Services.remove({category: categoryId});
            res.status(200).json({
                message: 'Category was deleting'
            })
        }
    } catch (err) {
        errorHendler(res, err);
    }
}

module.exports.createCategory = async function(req, res) {
    const name = req.body.name;
    console.log(name)
    const v = new Validator({name: name}, {
        name: 'required',
    });
    try {
        const matched = await v.check();
        if (!matched) {
            res.status(422).send(v.errors);
        } else {
            const category = new Category({
                name: name,
                user: req.user.id,
                imageSrc: req.file ? req.file.path : ''
            })
            await category.save();
            res.status(201).json(category)
        }
    } catch (err) {
        errorHendler(res, err);
    }
}

module.exports.updateCategory = async function(req, res) {
    const name = req.body.name;
    const id = req.params.id
    const v = new Validator({name: name, id: id}, {
        name: 'required',
        id: 'required'
    });
    let updated = {
        name: name
    };

    if (req.file) {
        updated.imageSrc = req.file.path
    }

    try {
        const matched = await v.check();
        if (!matched) {
            res.status(422).send(v.errors);
        } else {
            const category = await Category.findOneAndUpdate(
                {_id: id},
                {$set: updated},
                {new: true}
                )
            res.status(200).json(category);
        }
    } catch (err) {
        errorHendler(res, err);
    }
}