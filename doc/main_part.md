#### Определиться тему
#### Утвердить тему
#### Получить контакты рук-ля и руководство
#### Получить комплект мат-ов

## Тематика

Ключевое слово ***Автоматизация***


*Автоматизация управления взаимоотношениями с клиентами в компании ООО "РЕГ.РУ"*

#### Титульный лист
Уже заполнено, надо внести только свои данные

#### ЗАдание на дипломное проектирование
Нужно спросить в диконате № приказа и дату приказа
Срок сдачи берется из календарного плана (когда заверишь проект)

Исходные данные - (Исследование собираем исх инфу по фирме, выявляем слабые месте, требующие автоматизации, 

ТЗ для *Автоматизация управления взаимоотношениями с клиентами в компании ООО "РЕГ.РУ"*)

утверждено компанией

#### Календарный план

Дата - отталкиваясь от даты защиты

Календарный план - утверждается рук-лем дипломного проекта
после заполнения отправить на проверку рук-лю и согласовать дедлайны

## Структура ДП

- Титульный лист
- Календарный план
- Оглавление (содержание)
- Введение
- Первая , вторая, третья главы
- Заключение
- Список лит-ры
- Приложение (app)
- Последний лист ДП (шаблонный лист уже заполнен)

## Введение
1. Раскрыть актуальность темы (для конкретного предприятия)
2. Перечислить вопросы , которые будут рассмотрены в теме, акцент на тех, что будем решать
3. Определить цель ()
4. Обозначить решение()
5. Определить структуру ДП ()