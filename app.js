const path = require('path')
const express = require('express');
const passport = require('passport');
const authRouter = require('./routes/auth');
const categoryRouter = require('./routes/category');
const orderRouter = require('./routes/order');
const servicesRouter = require('./routes/services');
const statisticsRouter = require('./routes/statistics');
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongooseConnect = require('./utils/database');

const app = express();
mongooseConnect.mongooseConnect();

app.use(passport.initialize());
require('./middleware/passport')(passport);
app.use(morgan('dev'));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.use('/api/auth', authRouter);
app.use('/api/category', categoryRouter);
app.use('/api/order', orderRouter);
app.use('/api/services', servicesRouter);
app.use('/api/statistics', statisticsRouter);

if (process.env.NODE_ENV === 'production') {
    app.use(express.static('client/dist/client'));
    // For SPA
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'dist', 'client', 'index.html'))
    })
}

module.exports = app;