import { Component, OnInit } from '@angular/core';
import { AuthService } from './shared/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'client';
  constructor(private auth: AuthService) {
  }
  // Если обновить страничку, то токен улетит из хеддера
  // для этого достаем его из localStorage и записываем опять в сессию
  ngOnInit() {
    const potentialToken = localStorage.getItem('auth-token')
    if (potentialToken !== null ) {
      this.auth.setToken(potentialToken)
    }
  }
}
