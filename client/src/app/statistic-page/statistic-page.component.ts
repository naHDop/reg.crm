import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { StatisticsService } from '../shared/services/statistics.service';
import { AnaliticsPage } from '../shared/interfaces';
import { Chart } from 'chart.js'
import { Subscription } from 'rxjs';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-statistic-page',
  templateUrl: './statistic-page.component.html',
  styleUrls: ['./statistic-page.component.css']
})
export class StatisticPageComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('gain', {static: true}) gainRef: ElementRef
  @ViewChild('order', {static: true}) orderRef: ElementRef

  aSub:Subscription
  avarage: number
  panding = true
  isAdmin: boolean
  userRole: string


  constructor(private service: StatisticsService,
              private auth: AuthService,
              private router: Router) { }

  ngOnInit() {
    // this.isAdmin = this.auth.isAdmin()
    this.userRole = this.auth.getRole()
    if( this.userRole !== 'admin' && this.userRole !== 'stat' ) {
      this.router.navigate(['/history'])
    }
  }
  
  ngOnDestroy() {
    if (this.aSub) {
      this.aSub.unsubscribe()
    }
  }

  ngAfterViewInit() {
    const gainConfig: any = {
      label: 'Выручка',
      color: 'rgb(255, 99, 132)'
    }

    const orderConfig: any = {
      label: 'Заказы',
      color: 'rgb(54, 162, 235)'
    }
    this.aSub = this.service.getAnalitics().subscribe(
      (data: AnaliticsPage) => {
        this.avarage = data.avarage

        gainConfig.labels = data.chart.map(item => item.label)
        gainConfig.data = data.chart.map(item => item.gain)

        orderConfig.labels = data.chart.map(item => item.label)
        orderConfig.data = data.chart.map(item => item.order)

        // **** gain temp ****
        // gainConfig.labels.push('04.11.2019')
        // gainConfig.labels.push('05.11.2019')

        // gainConfig.data.push('1500')
        // gainConfig.data.push('21500')

        // **** gain temp ****

        // **** order temp ****
        // orderConfig.labels.push('04.11.2019')
        // orderConfig.labels.push('05.11.2019')

        // orderConfig.data.push(8)
        // orderConfig.data.push(4)

        // **** order temp ****

        const gainContext = this.gainRef.nativeElement.getContext('2d')
        const orderContext = this.orderRef.nativeElement.getContext('2d')
        gainContext.canvas.height = '300px'
        orderContext.canvas.height = '300px'

        new Chart(gainContext, createChartConfig(gainConfig))
        new Chart(orderContext, createChartConfig(orderConfig))

        this.panding = false
      }
    )
  }

}

function createChartConfig({labels, data, label, color}) {
  return {
    type: 'line',
    options: {
      responsive: true
    },
    data: {
      labels,
      datasets: [
        {
          label, data,
          borderColor: color,
          steppedLine: false,
          fill: false
        }
      ]
    }
  }
}
