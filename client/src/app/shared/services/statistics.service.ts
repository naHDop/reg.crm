import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OverviewPage, AnaliticsPage } from '../interfaces';

@Injectable({
    providedIn: 'root'
})
export class StatisticsService {
    constructor(private http: HttpClient) {
    }

    getOverview():Observable<OverviewPage> {
        return this.http.get<OverviewPage>('/api/statistics/overview')
    }
    
    getAnalitics():Observable<AnaliticsPage> {
        return this.http.get<AnaliticsPage>('/api/statistics/analitic')
    }
}