import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Service, Message } from '../interfaces';

@Injectable({
    providedIn: 'root'
})
export class ServiseService {
    constructor(private http: HttpClient) {
    }

    fetch(categoryId: string): Observable<Service[]> { 
        return this.http.get<Service[]>(`/api/services/${categoryId}`)
    }

    create(service: Service): Observable<Service> {
        return this.http.post<Service>('/api/services', service)
    }

    update(service: Service): Observable<Service> {
        return this.http.patch<Service>(`/api/services/${service._id}`, service)
    }

    delete(service: Service): Observable<Message> {
        return this.http.delete<Message>(`/api/services/${service._id}`)
    }
}