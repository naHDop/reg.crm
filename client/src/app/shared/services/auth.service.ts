import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../interfaces';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private token = null

    constructor(private http: HttpClient) {
    }

    registration(user: User): Observable<User> {
        return this.http.post<User>('/api/auth/register', user)
    }

    login(user: User): Observable<{ token: string }> {
        // http://localhost:<realport>/api/auth/login - backend
        return this.http.post<{ token: string }>('/api/auth/login', user)
            .pipe(
                tap(
                    ({ token }) => {
                        localStorage.setItem('auth-token', token)
                        this.setToken(token)
                    }
                )
            )
    }

    setToken(token: string) {
        this.token = token
    }

    getToken(): string {
        return this.token
    }

    isAuth(): boolean {
        // !! - приводим к белевому значению
        return !!this.token
    }

    isAdmin(): boolean {
        const decode = JSON.parse(atob(this.token.split('.')[1]));
        return decode.isAdmin
    }

    getRole(): string {
        const decode = JSON.parse(atob(this.token.split('.')[1]));
        return decode.role
    }

    logout() {
        this.setToken(null)
        localStorage.clear()
    }
}