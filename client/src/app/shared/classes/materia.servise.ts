import { ElementRef } from '@angular/core';

// Искусственная переменная, что бы TS не выдавал ошибки
declare var M

export interface MaterialInstance {
    open?(): void
    close?(): void
    destroy?(): void
}

export interface MaterialDatepicker extends MaterialInstance {
    date?: Date
}

export class MaterialService {
    static toast(message: string, color: string) {
        M.toast({html: message, classes: `${color} darken-1 rounded`})
    }

    static initializeFloatingButton(ref: ElementRef) {
        M.FloatingActionButton.init(ref.nativeElement);
    }

    static updateTextInputs() {
        M.updateTextFields()
    }

    static initModal(ref :ElementRef): MaterialInstance {
        return M.Modal.init(ref.nativeElement)
    }

    static initTooltip(ref: ElementRef): MaterialInstance {
        return M.Tooltip.init(ref.nativeElement);
    }

    static initDatepiker(ref: ElementRef, onClose: () => void): MaterialDatepicker {
        return M.Datepicker.init(ref.nativeElement, {
            format: 'dd.mm.yyyy',
            showClearButton: true,
            onClose
        });
    }

    static initSelect(ref: ElementRef): MaterialInstance {
        return M.FormSelect.init(ref.nativeElement);
    }

    static initTapTarget(ref: ElementRef): MaterialInstance {
        return M.TapTarget.init(ref.nativeElement)
    }
}