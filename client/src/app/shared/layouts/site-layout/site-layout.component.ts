import { Component, AfterViewInit, ViewChild, ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { MaterialService } from '../../classes/materia.servise';

@Component({
  selector: 'app-site-layout',
  templateUrl: './site-layout.component.html',
  styleUrls: ['./site-layout.component.css']
})
export class SiteLayoutComponent implements AfterViewInit, OnInit {

  @ViewChild('floating', { static: true }) floatingRef: ElementRef

  links = [
    // {url: '/overview', name: 'Обзор'},
    // {url: '/statistic', name: 'Статистика'},
    // {url: '/history', name: 'История'},
    // {url: '/order', name: 'Добавить заказ'},
    // {url: '/categories', name: 'Сервисы'},
  ]

  private roleLinks = {
    admin: [
      {url: '/overview', name: 'Обзор'},
      {url: '/statistic', name: 'Статистика'},
      {url: '/history', name: 'История'},
      {url: '/order', name: 'Добавить заказ'},
      {url: '/categories', name: 'Сервисы'},
    ],
    client: [
      {url: '/history', name: 'История'},
      {url: '/order', name: 'Добавить заказ'},
    ],
    stat: [
      {url: '/overview', name: 'Обзор'},
      {url: '/statistic', name: 'Статистика'},
      {url: '/history', name: 'История'},
    ],
    support: [
      {url: '/categories', name: 'Сервисы'},
      {url: '/history', name: 'История'},
    ]
    
  }

  isAdmin: boolean
  userRole: string

  constructor(private auth:AuthService,
              private router: Router) { }

  ngAfterViewInit(){
    MaterialService.initializeFloatingButton(this.floatingRef)
  }

  ngOnInit(){
    // this.isAdmin = this.auth.isAdmin()
    this.userRole = this.auth.getRole()
    this.links = this.roleLinks[this.userRole]
    }

  logout(event: Event) {
    event.preventDefault()
    this.auth.logout()
    this.router.navigate(['/login'])
  }



}
