import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit } from '@angular/core';
import { StatisticsService } from '../shared/services/statistics.service';
import { Observable } from 'rxjs';
import { OverviewPage } from '../shared/interfaces';
import { MaterialInstance, MaterialService } from '../shared/classes/materia.servise';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-overview-page',
  templateUrl: './overview-page.component.html',
  styleUrls: ['./overview-page.component.css']
})
export class OverviewPageComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('tapTarget',{static: true}) TapTargerRef: ElementRef
  tapTarget: MaterialInstance
  data$: Observable<OverviewPage>

  yesterday = new Date()
  isAdmin: boolean
  userRole: string

  constructor(private service: StatisticsService,
              private auth: AuthService,
              private router: Router) { }

  ngOnInit() {
      // this.isAdmin = this.auth.isAdmin()
      this.userRole = this.auth.getRole()
      if (this.userRole !== 'admin' && this.userRole !== 'stat') {
        this.router.navigate(['/history'])
      }
      this.data$ = this.service.getOverview()
      this.yesterday.setDate(this.yesterday.getDate() - 1)
  }

  ngOnDestroy() {
    this.tapTarget.destroy()
  }

  openInfo() {
    this.tapTarget.open()
  }

  ngAfterViewInit() {
    this.tapTarget = MaterialService.initTapTarget(this.TapTargerRef)
  }

}
