import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoriesService } from 'src/app/shared/classes/categories.service';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { MaterialService } from 'src/app/shared/classes/materia.servise';
import { Category } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-categories-form',
  templateUrl: './categories-form.component.html',
  styleUrls: ['./categories-form.component.css']
})
export class CategoriesFormComponent implements OnInit {

  @ViewChild('input', { static: true }) inputRef: ElementRef
  form: FormGroup
  image: File
  imagePreview
  isNew = true
  category: Category

  constructor(private route: ActivatedRoute,
    private categoriesService: CategoriesService,
    private router: Router) { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required)
    })
    this.form.disable()
    // Первый AJAX запрос
    this.route.params
      .pipe(
        switchMap(
          // Второй AJAX запрос
          (params: Params) => {
            if (params['id']) {
              this.isNew = false
              return this.categoriesService.getById(params['id'])
            }

            return of(null)
          }
        )
      ).subscribe(
        (category) => {
          if (category) {
            this.category = category
            this.form.patchValue({
              name: category.name
            })
            this.imagePreview = category.imageSrc
            MaterialService.updateTextInputs()
          }

          this.form.enable()
        },
        (error) => {
          MaterialService.toast(error.error.message, 'red')
        }
      )
  }

  triggerClick() {
    this.inputRef.nativeElement.click()
  }

  deleteCategory() {
    const decision = window.confirm(`Вы уерены, что хотите удалить категорию ${this.category.name}?`)

    if (decision) {
      this.categoriesService.delete(this.category._id)
      .subscribe(
        // В subscribe можно поместить 3 коллбека
        // 1 - если есть ошибка
        // 2 - если все ок
        // 3 - когда стрим был завершен
        (response) => {
          MaterialService.toast(response.message, 'green')
        },
        (error) => {
          MaterialService.toast(error.error.message, 'red')
        },
        () => {
          this.router.navigate(['/categories'])
        }
      )
    }
  }
  onFileUpload(event: any) {
    const file = event.target.files[0]
    this.image = file

    const reader = new FileReader()

    reader.onload = () => {
      this.imagePreview = reader.result
    }

    reader.readAsDataURL(file)
  }

  onSubmit() {
    let obs$
    this.form.disable()
    if (this.isNew) {
      //create
      obs$ = this.categoriesService.create(this.form.value.name, this.image)
    } else {
      // update
      obs$ = this.categoriesService.update(this.category._id, this.form.value.name, this.image)
    }

    obs$.subscribe(
      (category) => {
        this.category = category
        MaterialService.toast('Изменения сохранены', 'green')
        this.form.enable()
      },
      (error) => {
        MaterialService.toast(error.error.message, 'red')
        this.form.enable()
      }
    )
  }

}
