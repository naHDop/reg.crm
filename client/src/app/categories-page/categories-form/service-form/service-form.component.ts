import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { ServiseService } from 'src/app/shared/services/service.service';
import { Service } from 'src/app/shared/interfaces';
import { MaterialService, MaterialInstance } from 'src/app/shared/classes/materia.servise';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-service-form',
  templateUrl: './service-form.component.html',
  styleUrls: ['./service-form.component.css']
})
export class ServiceFormComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input('categoryId') categoryId: string
  @ViewChild('modal', { static: true }) modalRef: ElementRef

  services: Service[] = []
  loading = false
  serviceId = null
  modal: MaterialInstance
  form: FormGroup

  constructor(private serviceServise: ServiseService) {
   }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, Validators. required),
      cost: new FormControl(1, [Validators.required, Validators.min(1)])
    })
    this.loading = true
    this.serviceServise.fetch(this.categoryId)
      .subscribe(
        services => {
          this.services = services
          this.loading = false
        }
      )
  }

  ngOnDestroy() {
    this.modal.destroy()
  }

  ngAfterViewInit() {
    this.modal = MaterialService.initModal(this.modalRef)
  }

  onSelectService(service: Service){
    this.serviceId = service._id
    this.form.patchValue({
      name: service.name,
      cost: service.cost
    })
    this.modal.open()
    MaterialService.updateTextInputs()
  }

  onAddService() {
    this.serviceId = null
    this.form.reset({name: '', cost: 1})
    this.modal.open()
    MaterialService.updateTextInputs()
  }

  onDeleteService(event: Event, service: Service) {
    event.stopPropagation()
    const decision = window.confirm(`Удалить сервис ${service.name} ?`)

    if (decision) {
      this.serviceServise.delete(service).subscribe(
        res => {
          const idx = this.services.findIndex(s => s._id === service._id)
          this.services.splice(idx, 1)
          MaterialService.toast(res.message, 'green')
        },
        error => {
          MaterialService.toast(error.error.message, 'red')
        }
      )
    }
  }

  onCancel() {
    this.modal.close()
  }

  onSubmit() {
    this.form.disable()

    const newService: Service = {
      name: this.form.value.name,
      cost: this.form.value.cost,
      category: this.categoryId
    }

    const complited = () => {
      this.modal.close()
      this.form.reset({name: '', cost: 1})
      this.form.enable()
    }

    if (this.serviceId) {
      newService._id = this.serviceId
      this.serviceServise.update(newService).subscribe(
        (service) => {
          const idx = this.services.findIndex( s=> s._id === service._id)
          this.services[idx] = service
          MaterialService.toast('Изменения сохранены', 'green')
        },
        (error) => {
          this.form.enable()
          MaterialService.toast(error.error.message, 'red')
        },
        complited
      )
    } else {
      this.serviceServise.create(newService).subscribe(
        (service) => {
          MaterialService.toast('Сервис создан', 'green')
          this.services.push(service)
        },
        (error) => {
          this.form.enable()
          MaterialService.toast(error.error.message, 'red')
        },
        complited
      )
    }
  }

}
