import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../shared/classes/categories.service';
import { Category } from '../shared/interfaces';
import { Observable } from 'rxjs';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories-page',
  templateUrl: './categories-page.component.html',
  styleUrls: ['./categories-page.component.css']
})
export class CategoriesPageComponent implements OnInit {

  categories$: Observable<Category[]>

  isAdmin: boolean
  userRole: string

  constructor(private categoriesServise: CategoriesService,
              private auth: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    // this.isAdmin = this.auth.isAdmin()
    this.userRole = this.auth.getRole()
    if ( this.userRole !== 'admin' && this.userRole !== 'support' ) {
      this.router.navigate(['/history'])
    }
    this.categories$ = this.categoriesServise.fetch()
  }

}
