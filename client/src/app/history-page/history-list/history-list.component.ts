import { Component, OnInit, Input, ViewChild, ElementRef, OnDestroy, AfterViewInit } from '@angular/core';
import { Order } from 'src/app/shared/interfaces';
import { MaterialInstance, MaterialService } from 'src/app/shared/classes/materia.servise';
import { OrdersService } from 'src/app/shared/services/order.service';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-history-list',
  templateUrl: './history-list.component.html',
  styleUrls: ['./history-list.component.css']
})
export class HistoryListComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() orders: Order[]
  @ViewChild('modal', {static: true}) modalRef: ElementRef
  @ViewChild('modalFeedback', {static: true}) modalFeebackRef: ElementRef

  selectedOrder: Order
  orderId: string
  modal: MaterialInstance
  modalFeedback: MaterialInstance
  userRole: string


  feedbackMap = {
    '-2':'sentiment_very_dissatisfied',
    '0': 'sentiment_neutral',
    '2': 'sentiment_very_satisfied'
  }

  constructor(private orederService: OrdersService,
              private auth: AuthService) { }

  ngOnInit() {
    this.userRole = this.auth.getRole()
  }

  ngOnDestroy() {
    this.modal.destroy()
    this.modalFeedback.destroy()
  }

  ngAfterViewInit() {
    this.modal = MaterialService.initModal(this.modalRef)
    this.modalFeedback = MaterialService.initModal(this.modalFeebackRef)
  }

  computePrice(order: Order): number {
    return order.list.reduce((total, item) => {
      return total += item.quantity * item.cost
    }, 0)
  }

  addFeddback(orderId) {
    this.orderId = orderId
    this.modalFeedback.open()
  }

  estimate(feedbackEstimation){
    this.orederService.estimateOrder(this.orderId, feedbackEstimation).subscribe(
      () => {
        MaterialService.toast('Вы оценили заказ', 'green')
      },
      (error) => {
        MaterialService.toast(error.error.message, 'red')
      },
      () => {
        this.modalFeedback.close()
        window.location.reload()
      }
    )

  }

  selectOrder(order: Order) {
    this.selectedOrder = order
    this.modal.open()
  }

  closeFeedbackModal() {
    this.modalFeedback.close()
  }

  closeModal() {
    this.modal.close()
  }

}
