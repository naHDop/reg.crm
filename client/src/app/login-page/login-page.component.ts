import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../shared/services/auth.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MaterialService } from '../shared/classes/materia.servise';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit, OnDestroy {

  form: FormGroup
  aSub: Subscription

  constructor(private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)])
    });

    this.route.queryParams.subscribe((params: Params) => {
      if (params['registered']) {
        MaterialService.toast('Можете зайти в систему', 'green')
      } else if (params['accessDenied']) {
        MaterialService.toast('Сначала авторизуйтесь', 'red')
      } else if (params['sessionFaild']) {
        MaterialService.toast('Пожалуйста войдите заново', 'red')
      }
    })
  }

  ngOnDestroy() {
    if(this.aSub) {
      this.aSub.unsubscribe()
    }
  }

  onSubmit() {
    const user = {
      email: this.form.value.email,
      password: this.form.value.password,
    }
    this.form.disable()
    // this.form.value
    this.aSub = this.auth.login(user).subscribe(
      () => {this.router.navigate(['/overview'])},
      (error) => {
        MaterialService.toast(error.error.message, 'red')
        this.form.enable()
      }
    )
  }

}
