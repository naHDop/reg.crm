import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ServiseService } from 'src/app/shared/services/service.service';
import { Observable } from 'rxjs';
import { Service } from 'src/app/shared/interfaces';
import { switchMap, map } from 'rxjs/operators';
import { OrderService } from '../order.service';
import { MaterialService } from 'src/app/shared/classes/materia.servise';

@Component({
  selector: 'app-order-services',
  templateUrl: './order-services.component.html',
  styleUrls: ['./order-services.component.css']
})
export class OrderServicesComponent implements OnInit {

  services$: Observable<Service[]>

  constructor(private route: ActivatedRoute,
              private ServicesService: ServiseService,
              private order: OrderService) { }

  ngOnInit() {
    this.services$ = this.route.params
      .pipe(
        switchMap(
          (params: Params) => {
            return this.ServicesService.fetch(params['id'])
          }
        ),
        map(
          (service: Service[]) => {
            return service.map(serv => {
              serv.quantity = 1
              return serv
            })
          }
        )
      )
  }

  addToOrder(service: Service) {
    MaterialService.toast(`Элемент добавлен x${service.quantity} к заказу`, 'green');
    this.order.add(service)
  }

}
