import { Injectable } from '@angular/core';
import { Service, OrderPosition } from '../shared/interfaces';

@Injectable()
export class OrderService {

    public list: OrderPosition[] = []
    public price = 0

    add(service: Service) {
        const orderPos: OrderPosition = Object.assign({}, {
            name: service.name,
            cost: service.cost,
            quantity: service.quantity,
            _id: service._id
        })

        const condidate = this.list.find( s => s._id === orderPos._id)

        if (condidate) {
            // quantity ++
            condidate.quantity += orderPos.quantity
        } else {
            // add new position
            this.list.push(orderPos)
        }
        this.computePrice()
    }

    remove(orderPosition: OrderPosition) {
        const idx = this.list.findIndex( p => p._id === orderPosition._id)
        this.list.splice(idx, 1)
        this.computePrice()
    }

    clear() {
        this.list = []
        this.price = 0
    }

    private computePrice() {
        this.price = this.list.reduce((total, item) => {
            return total += item.quantity * item.cost
        }, 0)
    }
}