const bcryptjs = require('bcryptjs');

module.exports.HashedSync = function(password) {
    const salt = bcryptjs.genSaltSync(10);
    return bcryptjs.hashSync(password, salt);
}

module.exports.compareSync = function(condidatePass, realPass) {
    return bcryptjs.compareSync(condidatePass, realPass);
}