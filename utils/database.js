require('dotenv').config();
const mongoose = require('mongoose');

const mongooseConnect = async () => {
    try {
        await mongoose.connect(
            process.env.DB_CONNECT,
            { 
                useNewUrlParser: true, 
                useUnifiedTopology: true,
                useFindAndModify: false
            }
        );
        console.log('Mongoose is connected ! ')
    } catch (err) {
        console.log(err);
    }
};

exports.mongooseConnect = mongooseConnect;